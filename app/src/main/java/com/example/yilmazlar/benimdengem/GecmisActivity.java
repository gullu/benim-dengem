package com.example.yilmazlar.benimdengem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class GecmisActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gecmis);

        ListView listView = (ListView)findViewById(R.id.liste);
        Database db =new Database(getApplicationContext());


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                db.getResultIntakeInfo());
        listView.setAdapter(arrayAdapter);

    }
}
