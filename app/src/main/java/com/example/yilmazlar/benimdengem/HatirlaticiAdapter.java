package com.example.yilmazlar.benimdengem;

/**
 * Created by YILMAZLAR on 4.12.2016.
 */

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import static java.util.Collections.addAll;

public class HatirlaticiAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<Hatirlatici>    mHatirlatici;

    private Context context;

    public HatirlaticiAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getCount(){
        return mHatirlatici.size();
    }
    @Override
    public Hatirlatici getItem(int position){
        return mHatirlatici.get(position);
    }
    @Override
    public  long getItemId(int position){
        return position;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent){
        final View satirView;

        satirView=mInflater.inflate(R.layout.hatirlatma_adi_list,null);
        TextView textView=(TextView)satirView.findViewById(R.id.textView3);
        TextView textView1=(TextView)satirView.findViewById(R.id.textView4);




        Hatirlatici hatirlatici=mHatirlatici.get(position);

        textView.setText(hatirlatici.getHatirlaticiAdi());
        textView1.setText(hatirlatici.getGunBolumu());


        android.support.v7.widget.SwitchCompat iswitch = (SwitchCompat) satirView.findViewById(R.id.switch1);

        iswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if (b) {

                            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            Intent intent = new Intent(context, AlarmReceiver.class);
                            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);


                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(System.currentTimeMillis());
                            calendar.set(Calendar.HOUR_OF_DAY, 16);
                            calendar.set(Calendar.MINUTE, 26);


                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                                    AlarmManager.INTERVAL_DAY, alarmIntent);
                            Toast.makeText(context, "check", Toast.LENGTH_SHORT).show();


                        } else {

                            Toast.makeText(context, "check dışı", Toast.LENGTH_SHORT).show();

                        }
                    }


        });


return satirView;
    }

    public HatirlaticiAdapter (Activity activity, List<Hatirlatici>itemler){
        mInflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mHatirlatici=itemler;
        addAll(mHatirlatici);
    }


}





