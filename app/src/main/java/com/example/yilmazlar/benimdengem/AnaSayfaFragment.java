package com.example.yilmazlar.benimdengem;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class AnaSayfaFragment extends Fragment {


    public AnaSayfaFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view= inflater.inflate(R.layout.fragment_ana_sayfa, container, false);


       FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.gida_arat_floating);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inn = new Intent(v.getContext(),BarcodeScanner.class);
                startActivity(inn);
            }
        });

        return view;
    }



}
