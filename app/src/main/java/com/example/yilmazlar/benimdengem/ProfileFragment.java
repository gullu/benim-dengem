package com.example.yilmazlar.benimdengem;


import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ProfileFragment extends Fragment {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private int sexId;
    public ProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);


        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        String  ad = mFirebaseUser.getDisplayName().toString();
        ImageView ımageView=(ImageView)view.findViewById(R.id.imageView);

        if (mFirebaseUser.getPhotoUrl() != null) {
            String mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();

            ımageView.setImageBitmap(getBitmapFromURL(mPhotoUrl));
        }



        TextView cinsiyet =(TextView)view.findViewById(R.id.textView);
        TextView yas = (TextView)view.findViewById(R.id.textView2);
        final TextView boy = (TextView)view.findViewById(R.id.textView3);
        final TextView sonuc = (TextView)view.findViewById(R.id.textView6);
        final EditText boyGir = (EditText)view.findViewById(R.id.editText2);
        Button hesapla = (Button)view.findViewById(R.id.button);
        final EditText agirlikGir =(EditText)view.findViewById(R.id.editText3);
        final EditText yasGir = (EditText)view.findViewById(R.id.editText);
        RadioGroup sexGroup = (RadioGroup) view.findViewById(R.id.settings_sexRadiogroup);
        final RadioButton erkek = (RadioButton) view.findViewById(R.id.radioButton2);
        RadioButton kadin = (RadioButton) view.findViewById(R.id.radioButton);



        SavedData data = new SavedData();
        if (data.getStartedVarsCreated(getActivity())) {

            if (data.getSex(getActivity()) == 1) {
                sexGroup.check(R.id.radioButton2);
                sexId = 1;
            } else if (data.getSex(getActivity()) == 2) {
                sexGroup.check(R.id.radioButton);
                sexId = 2;
            }



            yasGir.setText(String.valueOf(data.getAge(getActivity())));
            agirlikGir.setText(String.valueOf(data.getWeight(getActivity())));
            boyGir.setText(String.valueOf(data.getHeight(getActivity())));
            sonuc.setText(String.valueOf(data.getDailyNeeds(getActivity())));


        }


        sexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (erkek.isChecked()) {
                    sexId = 1;
                } else {
                    sexId = 2;
                }


            }
        });

        hesapla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sexId == 0 ||
                        yasGir.getText().toString().trim().length() == 0 ||
                        agirlikGir.getText().toString().trim().length() == 0 ||
                        boyGir.getText().toString().trim().length() == 0
                        ) {

                    Toast toast = Toast.makeText(getContext(),
                            "Lütfen Değerlerin Hepsini Giriniz!", Toast.LENGTH_SHORT);
                    toast.show();

                } else {

                    if (!yasGir.getText().toString().matches("^[0-9]*$") ||
                            !agirlikGir.getText().toString().matches("^[0-9]*$") ||
                            !boyGir.getText().toString().matches("^[0-9]*$")
                            ) {

                        Toast toast = Toast.makeText(getContext(),
                                "Lütfen doğru değerleri girin!", Toast.LENGTH_SHORT);
                        toast.show();


                    } else {


                        SavedData data = new SavedData();
                        data.setStartedVarsCreated(getContext(), true);
                        data.setSex(getContext(), sexId);
                        data.setAge(getContext(), Integer.parseInt(yasGir.getText().toString()));
                        data.setWeight(getContext(), Integer.parseInt(agirlikGir.getText().toString()));
                        data.setHeight(getContext(), Integer.parseInt(boyGir.getText().toString()));


                        if (sexId == 1) {
                            double needs = (10 * Integer.parseInt(agirlikGir.getText().toString()) +
                                    6.25 * Integer.parseInt(boyGir.getText().toString()) -
                                    5 * Integer.parseInt(yasGir.getText().toString()) + 5) * 1.2;
                            data.setDailyNeeds(getContext(), (int) needs);
                            sonuc.setText(String.valueOf((int) needs));
                        } else if (sexId == 2) {
                            double needs = (10 * Integer.parseInt(agirlikGir.getText().toString()) +
                                    6.25 * Integer.parseInt(boyGir.getText().toString()) -
                                    5 * Integer.parseInt(yasGir.getText().toString()) - 161) * 1.2;
                            data.setDailyNeeds(getContext(), (int) needs);
                            sonuc.setText(String.valueOf((int) needs));


                        }

                    }
                }
            }
        });

      //  Intent in = new Intent(getContext(),GunlukActivity.class);
     //   in.putExtra("sonuc",sonuc.getText().toString());
    //    startActivity(in);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("sonuc",sonuc.getText().toString());
        editor.commit();


        return view;


    }


    public static Bitmap getBitmapFromURL(String src) {
        try {

            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Exception",e.getMessage());
            return null;
        }
    }

}