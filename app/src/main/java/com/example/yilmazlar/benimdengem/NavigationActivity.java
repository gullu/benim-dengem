package com.example.yilmazlar.benimdengem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashMap;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    UserSessionManager session;
    GidaAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        setTitle("Benim Dengem");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        session = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();



        // get name
        String name = user.get(UserSessionManager.username);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){

        getMenuInflater().inflate(R.menu.navigation, menu);
      //  getMenuInflater().inflate(R.menu.barcode_ara, menu);
      //  getMenuInflater().inflate(R.menu.menu_search,menu);
/*
        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);

        searchView.setOnQueryTextListener(this);
*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.cikis_yap) {

            session.logoutUser();
           // Intent ıntent = new Intent(this,Twitter.class);
           // startActivity(ıntent);
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();



        if (id == R.id.ana_sayfa) {

                setTitle("Ana Sayfa");
                AnaSayfaFragment anaSayfaFragment = new AnaSayfaFragment();
                FragmentManager fragmentManager=getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.rl_content,anaSayfaFragment,anaSayfaFragment.getTag()).commit();

        }
        else if (id == R.id.gida_listesi) {

            setTitle("Gıda Listesi");
            GidaEkleFragment gidaEkleFragment = new GidaEkleFragment();
            FragmentManager fragmentManager=getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.rl_content,gidaEkleFragment,gidaEkleFragment.getTag()).commit();}

        else if (id == R.id.hatirlatmalar) {

            setTitle("Hatırlatmalar");
            Intent ıntent=new Intent(this,HatirlatmaActivity.class);
            ıntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            ıntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(ıntent);
            finish();

        }

        else if (id == R.id.kayit_ekle) {

            setTitle("Kayıt Ekle");
            KayitEkleFragment kayitEkleFragment=new KayitEkleFragment();
            FragmentManager fragmentManager=getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.rl_content,kayitEkleFragment,kayitEkleFragment.getTag()).commit();

        } else if (id == R.id.gunluk) {

          //  setTitle("Günlük");
          //  GunlukFragment gunlukFragment=new GunlukFragment();
            //FragmentManager fragmentManager=getSupportFragmentManager();
            //fragmentManager.beginTransaction().replace(R.id.rl_content,gunlukFragment,gunlukFragment.getTag()).commit();

            Intent inte = new Intent(getApplication(),GunlukActivity.class);
            inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(inte);


        } else if (id == R.id.profil_id) {

            setTitle("Profil");
            ProfileFragment profileIdFragment = new ProfileFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.rl_content,profileIdFragment, profileIdFragment.getTag()).commit();

        }

         else if (id == R.id.cikis_yap) {
            session.logoutUser();
            finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}