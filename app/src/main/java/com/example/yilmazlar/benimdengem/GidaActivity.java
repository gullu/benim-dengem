package com.example.yilmazlar.benimdengem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.example.yilmazlar.benimdengem.GidaAdapter.getBitmapFromURL;


/**
 * Created by YILMAZLAR on 20.12.2016.
 */
public class GidaActivity extends AppCompatActivity {
    String gidaAdiBilgisi;
    String gidaResimBilgisi;
    String  calorie;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gida_activity);
        setTitle("Gıda Bilgisi");
        Intent in = getIntent();
        ImageView ımageView = (ImageView)findViewById(R.id.gidaresmi);
        TextView gidaAdi = (TextView)findViewById(R.id.textView7);
        final TextView calori =(TextView)findViewById(R.id.textView8);
        Button gunlugeEkle = (Button)findViewById(R.id.button2);


        gidaResimBilgisi=in.getStringExtra("gidaResimBilgisi");
        gidaAdiBilgisi=in.getStringExtra("gidaAdiBilgisi");
        calorie=in.getStringExtra("gidaCalorisi");






        gidaAdi.setText(gidaAdiBilgisi);
        calori.setText(calorie);
        ımageView.setImageBitmap(getBitmapFromURL(gidaResimBilgisi));


        gunlugeEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GidaActivity.this, GunlukActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // intent.putExtra("gidaAdi",gidaAdiBilgisi);
              //  intent.putExtra("giaResimiBilgisi",gidaResimBilgisi);
              //  intent.putExtra("kalori",String.valueOf(calori));
                Database db = new Database(getApplicationContext());
                String date = getDate();
                db.GidaEkle(date,gidaAdiBilgisi,calorie);
                db.close();

                startActivity(intent);
            }
        });

    }
    public String getDate()
    {
        SimpleDateFormat newDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        return newDate.format(new Date());
    }
}
