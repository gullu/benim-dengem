package com.example.yilmazlar.benimdengem;

/**
 * Created by YILMAZLAR on 12.12.2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;


public class GidaAdapter extends BaseAdapter {

    Context c;
    List<Gida> gidalar;
    List<Gida> kgidalar;



    @Override
    public int getCount() {
        return gidalar.size();
    }

    @Override
    public Gida getItem(int position) {return gidalar.get(position);}


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(c).inflate(R.layout.gida_adi_list, parent, false);
        }
        TextView nameTxt = (TextView) convertView.findViewById(R.id.gida_adi);
        ImageView ımageView = (ImageView) convertView.findViewById(R.id.gidaresmi);
        TextView textView = (TextView)convertView.findViewById(R.id.textView10);

        Gida gida = gidalar.get(position);

        String gidaAdi = gida.getFoods_name();
        String gidaResimi = gida.getImage();

        nameTxt.setText(gidaAdi);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("gidaAdi",gidaAdi);
        editor.putString("gidaResim",gidaResimi);
        editor.commit();



         String gidaAdii = preferences.getString("gidaAdi",null);


        //gidaResimii=preferencess.getString("gidaResim",null);
        if(textView==null)
        textView.setText(gidaAdii);



        if (gidaResimi != null) {
            String mPhotoUrl = gidaResimi.toString();

            //   Toast.makeText(c,mPhotoUrl , Toast.LENGTH_SHORT).show();

            ımageView.setImageBitmap(getBitmapFromURL(mPhotoUrl));

            //     Toast.makeText(c, "setImage", Toast.LENGTH_LONG).show();
        } else {


            //    Toast.makeText(c, "else", Toast.LENGTH_LONG).show();
        }


        return convertView;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {

            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Exception",e.getMessage());
            return null;
        }
    }


    public GidaAdapter (Context c, List<Gida> gidalar){

        this.c = c;
        this.gidalar = gidalar;

    }

    public void filter(String charText){
        charText=charText.toLowerCase(Locale.getDefault());
        gidalar.clear();

        if (charText.trim().length()==0){
            gidalar.addAll(kgidalar);

        }else {
            charText=charText.replace(".","");

            for (Gida v0 : kgidalar) {
                if (v0.getFoods_name().toLowerCase().trim().startsWith(charText.trim())){
                    gidalar.add(v0);
                    continue;
                }
                String [] splites= v0.getFoods_name().toLowerCase().trim().split("\\s++");

                if (splites!= null)
                {
                    for (String s: splites){
                        if (s.startsWith(charText.trim())){
                            gidalar.add(v0);
                            break;
                        }
                    }
                }
            }
            notifyDataSetChanged();

        }
    }

}

