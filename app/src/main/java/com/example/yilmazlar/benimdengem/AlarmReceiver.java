package com.example.yilmazlar.benimdengem;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by YILMAZLAR on 20.12.2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "onReceive", Toast.LENGTH_SHORT).show();
        Uri sesDosyasi = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager by = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification bilgi = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            bilgi = new Notification.Builder(context)
                    .setContentTitle("Benim Dengem 2")
                    .setAutoCancel(true)
                    .setContentText("Yemek zamanı 2")
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setSound(sesDosyasi)
                    .build();
            by.notify(0,bilgi);}

    }
}