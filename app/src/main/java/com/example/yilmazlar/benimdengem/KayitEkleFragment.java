package com.example.yilmazlar.benimdengem;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class KayitEkleFragment extends Fragment {

    ImageButton imagebutton;



    public KayitEkleFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_kayit_ekle,container,false);


        imagebutton=(ImageButton)view.findViewById(R.id.bttn_barkod_ara);
        imagebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Intent intent = new Intent(v.getContext(), BarcodeScanner.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
