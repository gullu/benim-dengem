package com.example.yilmazlar.benimdengem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class GunlukActivity extends AppCompatActivity {


    ArrayAdapter<String> adapter;
    ArrayList<HashMap<String, String>> food_liste;
    String gida_adlari[];
    int gida_idler[];
    TextView gunlukKaloriihtiyaci;
    TextView toplamKaloriMiktari;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_gunluk);

        Button gecmis = (Button)findViewById(R.id.button3);
        TextView tarih = (TextView)findViewById(R.id.main_current_date);
        TextView bilgi = (TextView)findViewById(R.id.main_exceeded);
        gunlukKaloriihtiyaci = (TextView)findViewById(R.id.textView12);
        setTitle("Günlük");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toplamKaloriMiktari = (TextView)findViewById(R.id.textView18);

        Database db =new Database(getApplicationContext());
        toplamKaloriMiktari.setText( db.toplamKalori());



        TextView kalanKaloriMiktari = (TextView)findViewById(R.id.textView16);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String sonuc = preferences.getString("sonuc",null);
        gunlukKaloriihtiyaci.setText(sonuc);

          tarih.setText(getDate());

        String gki =gunlukKaloriihtiyaci.getText().toString();


        DecimalFormat df = new DecimalFormat("####0.00");

        if(!gki.equals("") ) {

            if (toplamKaloriMiktari.getText().toString().equals("")) {

                Double sayi = 0.0;
                String hesap = String.valueOf((Double.valueOf(gunlukKaloriihtiyaci.getText().toString()) -sayi));
                kalanKaloriMiktari.setText(String.valueOf(df.format(Double.parseDouble(hesap))));

            }else
            {
                Toast.makeText(this, toplamKaloriMiktari.getText().toString(), Toast.LENGTH_SHORT).show();
                String hesap = String.valueOf((Double.valueOf(gunlukKaloriihtiyaci.getText().toString())-Double.valueOf(toplamKaloriMiktari.getText().toString())));
                kalanKaloriMiktari.setText(String.valueOf(df.format(Double.parseDouble(hesap))));
            }

        }
        else {
            kalanKaloriMiktari.setText(String.valueOf(0.0));
        }




        if((kalanKaloriMiktari.getText().toString().startsWith("-"))){

            bilgi.setText("Sınırı Geçtin, Dikkat !!!");
            bilgi.setTextColor(Color.parseColor("#e40114"));
        } else
        {
            bilgi.setText("Gıda Almaya Devam Edebilirsin !");
            bilgi.setTextColor(Color.parseColor("#177b03"));
        }
/*
        if(Double.parseDouble(kalanKaloriMiktari.getText().toString().trim())<=0){
            bilgi.setText("Sınırı Geçtin, Dikkat !!!");
            bilgi.setTextColor(Color.parseColor("#e40114"));
        }
        else {
            bilgi.setText("Gıda Almaya Devam Edebilirsin !");
            bilgi.setTextColor(Color.parseColor("#177b03"));
        }
*/
         //   if (gunlukKaloriihtiyaci.getText().toString()!= null);
        //    kalanKaloriMiktari.setText(String.valueOf(hesap()));

        gecmis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in =new Intent(getApplication(), GecmisActivity.class);
                startActivity(in);
            }
        });

    }
    public String getDate()
    {
        SimpleDateFormat newDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        return newDate.format(new Date());
    }

    @Override
    public void onResume(){
        super.onResume();
        Database db = new Database(getApplicationContext());
        food_liste = db.gidalar();
        if(food_liste.size()==0){
            Double sayi = 0.0;
            toplamKaloriMiktari.setText(String.valueOf(sayi));
            Toast.makeText(getApplicationContext(), "Henüz Gıda Eklenmemiş.", Toast.LENGTH_LONG).show();
        }else{
            gida_adlari = new String[food_liste.size()];
            gida_idler = new int[food_liste.size()];
            for(int i=0;i<food_liste.size();i++){
                gida_adlari[i] = food_liste.get(i).get("gida_adi");
                gida_idler[i] = Integer.parseInt(food_liste.get(i).get("id"));
            }
            ListView list = (ListView)findViewById(R.id.main_chosen_product_list);
            adapter = new ArrayAdapter<String>(this, R.layout.activity_gunluk_gida_adapter, R.id.textView11, gida_adlari);
            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Database db =new Database(getApplicationContext());
                    db.GidaSil(Integer.parseInt(food_liste.get(i).get("id")));
                }
            });


        }


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
