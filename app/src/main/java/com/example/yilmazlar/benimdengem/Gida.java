package com.example.yilmazlar.benimdengem;

public class Gida {

    private String foods_name;
    private String image;
    private long foods_barcode;
    private double calorie;


    public  Gida(){};

    public Gida(String foods_name, String image , long foods_barcode , double calorie) {
        super();
        this.foods_name = foods_name;
        this.image = image;
        this.foods_barcode=foods_barcode;
        this.calorie=calorie;
    }

    public String getFoods_name() {
        return foods_name;
    }

    public double getCalorie() {
        return calorie;
    }

    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }

    public void setFoods_name(String foods_name) {

        this.foods_name = foods_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public long getFoods_barcode() {
        return foods_barcode;
    }

    public void setFoods_barcode(long foods_barcode) {
        this.foods_barcode = foods_barcode;
    }
}