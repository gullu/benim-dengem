package com.example.yilmazlar.benimdengem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class HatirlatmaActivity extends AppCompatActivity
{

    HatirlaticiAdapter adapter;

    final java.util.List<Hatirlatici> hatirlaticilar = new ArrayList<Hatirlatici>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hatirlatma);
        setTitle("Hatırlatmalar");




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        hatirlaticilar.add(new Hatirlatici("Kahvaltı","Hergün,9.50"));
        hatirlaticilar.add(new Hatirlatici("Ara Öğün-Sabah","Hergün,11.50"));
        hatirlaticilar.add(new Hatirlatici("Öğle Yemeği","Hergün,14.50"));
        hatirlaticilar.add(new Hatirlatici("Ara Öğün-Öğleden Sonra","Hergün,16.50"));
        hatirlaticilar.add(new Hatirlatici("Akşam Yemeği","Hergün,20.20"));
        hatirlaticilar.add(new Hatirlatici("Ara Öğün-Gece","Hergün, 22.00"));



        final ListView List = (ListView)findViewById(R.id.liste);

        adapter = new HatirlaticiAdapter(this, hatirlaticilar);
        List.setAdapter(adapter);

        List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {


                Intent intent = new Intent(getApplicationContext(), Hatirlatici.class);

                intent.putExtra("hatirlaticiAdi", hatirlaticilar.get(position).getHatirlaticiAdi());
                intent.putExtra("gunBolumu", hatirlaticilar.get(position).getGunBolumu());


                startActivity(intent);

            }
        });

    }

   @Override
    public void onBackPressed() {
            Intent inn = new Intent(HatirlatmaActivity.this,NavigationActivity.class);
            startActivity(inn);
          //  super.onBackPressed();
       finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
