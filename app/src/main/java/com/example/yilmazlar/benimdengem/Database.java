package com.example.yilmazlar.benimdengem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by YILMAZLAR on 22.12.2016.
 */

public class Database extends SQLiteOpenHelper {

    Context co;


    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "gunluk_database";//database adý

    private static final String TABLE_NAME = "gunluk_gida_listesi";
    private static String GİDA_ADİ = "gida_adi";
    private static String GİDA_ID = "id";
    private static String GIDA_CALORİSİ = "calori";
    private static String DATE = "date";




    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + GİDA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + GİDA_ADİ + " TEXT,"
                + GIDA_CALORİSİ + " TEXT,"
                + DATE + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void GidaSil(int id) { //id ye göre silme işlemini gerçekleştiriyoruz.

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, GİDA_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void GidaEkle(String date ,String gida_adi, String kalori) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DATE, date);
        values.put(GİDA_ADİ, gida_adi);
        values.put(GIDA_CALORİSİ,kalori);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }
    public ArrayList<HashMap<String, String>> gidalar() {



        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<HashMap<String, String>> gidalist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                gidalist.add(map);
            } while (cursor.moveToNext());
        }
        db.close();

        return gidalist;
    }


    public String toplamKalori(){

        SQLiteDatabase dp = this.getReadableDatabase();
        String result = null;
        String value;
        String selectQuery = "SELECT SUM(calori) FROM " + TABLE_NAME;
        Cursor c = dp.rawQuery(selectQuery, null);
        if(c != null && c.moveToFirst()) {
            int i = 0;
            do {
                value = c.getString(0) ;
                result=(value);
            }
            while (c.moveToNext());

        }

        if (c != null) {
            c.close();
        }


        return result;



    }

    public ArrayList<String> getResultIntakeInfo()
    {
        SQLiteDatabase dp = this.getReadableDatabase();
        ArrayList<String> result = new ArrayList<>();
        String value;
        String selectQuery = "SELECT date, SUM(calori) FROM " + TABLE_NAME;
        Cursor c = dp.rawQuery(selectQuery, null);

        if(c != null && c.moveToFirst()) {
            int i = 0;
            do {
                value = c.getString(0) + "\n" + c.getString(1);
                result.add(i,value);
            }
            while (c.moveToNext());

        }

        if (c != null) {
            c.close();
        }


        return result;

    }

}
