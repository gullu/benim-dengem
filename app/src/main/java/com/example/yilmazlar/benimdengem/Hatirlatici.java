package com.example.yilmazlar.benimdengem;

/**
 * Created by YILMAZLAR on 4.12.2016.
 */
public class Hatirlatici {

    private String hatirlaticiAdi;
    private String gunBolumu;

    public String getHatirlaticiAdi() {
        return hatirlaticiAdi;
    }

    public String getGunBolumu() {
        return gunBolumu;
    }


    public Hatirlatici(String hatirlaticiAdi, String gunBolumu){
        super();
        this.hatirlaticiAdi=hatirlaticiAdi;
        this.gunBolumu=gunBolumu;


    }

    public void setHatirlaticiAdi(String hatirlaticiAdi) {
        this.hatirlaticiAdi = hatirlaticiAdi;
    }

    public void setGunBolumu(String gunBolumu) {
        this.gunBolumu = gunBolumu;
    }



}

